package uvsq21304441.exoForme;

import static org.junit.Assert.*;

import org.junit.Test;

public class RectangleTest {

	@Test
	public void test() {
		Rectangle r = new Rectangle();
		Rectangle r1 = new Rectangle(new Point(4,4));
		Rectangle r2 = new Rectangle(new Point(4,4),new Point(10,10));
		System.out.println(r.getBD().getX());
		assertEquals(r.getBD().getX(),0.0);
		assertEquals(r.getBD().getY(),0.0);
		assertEquals(r.getHG().getX(),0.0);
		assertEquals(r.getHG().getY(),0.0);
		assertEquals(r1.getHG().getX(),4.0);
		assertEquals(r1.getHG().getY(),4.0);
		assertEquals(r1.getBD().getX(),0.0);
		assertEquals(r1.getBD().getY(),0.0);
		assertEquals(r2.getHG().getX(),4.0);
		assertEquals(r2.getHG().getY(),4.0);
		assertEquals(r2.getBD().getX(),10.0);
		assertEquals(r2.getBD().getY(),10.0);
	}

}
