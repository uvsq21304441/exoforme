package uvsq21304441.exoForme;

import static org.junit.Assert.*;

import org.junit.Test;

public class PointTest {

	@Test
	public void test() {
		Point p = new Point();
		Point p1 = new Point(2.0);
		Point p2 = new Point(2.0,5.0);
		assertEquals(p.getX(),0.0);
		assertEquals(p.getY(),0.0);
		assertEquals(p1.getX(),2.0);
		assertEquals(p1.getY(),0.0);
		assertEquals(p2.getX(),2.0);
		assertEquals(p2.getY(),5.0);
	}

}
