package uvsq21304441.exoForme;

import static org.junit.Assert.*;

import org.junit.Test;

public class RondTest {

	@Test
	public void test() {
		Rond r = new Rond();
		Rond r1 = new Rond(new Point());
		Rond r2 = new Rond(4.0);
		Rond r3 = new Rond(4.0,new Point());
		
		
		assertEquals(r.getCentre().getX(),0.0);
		assertEquals(r.getCentre().getY(),0.0);
		assertEquals(r.getRayon(),1.0);
		
		assertEquals(r1.getCentre().getX(),0.0);
		assertEquals(r1.getCentre().getY(),0.0);
		assertEquals(r1.getRayon(),1.0);
		
		assertEquals(r2.getCentre().getX(),0.0);
		assertEquals(r2.getCentre().getY(),0.0);
		assertEquals(r2.getRayon(),4.0);	
		
		assertEquals(r3.getCentre().getX(),0.0);
		assertEquals(r3.getCentre().getY(),0.0);
		assertEquals(r3.getRayon(),4.0);	
		}

}
