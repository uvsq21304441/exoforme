package uvsq21304441.exoForme;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class RegroupTest {

	@Test
	public void test() {
		List<Forme> Global = new ArrayList<Forme>();
		List<Forme> Select = new ArrayList<Forme>();

		 	Rond r = new Rond();
			Rond r1 = new Rond(4,new Point(5,5));
			Rond r2 = new Rond(4, new Point(10,11));
			Rectangle r3 = new Rectangle(new Point(), new Point(8,8));
		
			Global.add(r);
			Global.add(r1);
			Global.add(r2);
			Global.add(r3);
		
			String[] test= new String[2];
			test[0]="0";
			test[1]="2";
			
			FormeTUI ui = new FormeTUI();
		Commande c = new Regroup(test);
		assertEquals(c.execute(Global, Select),"Les objets ont bien ete regroupes \n");
		assertEquals(Select.size(),2);
		
	}

}
