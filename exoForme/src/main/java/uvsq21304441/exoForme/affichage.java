package uvsq21304441.exoForme;

import java.util.ArrayList;
import java.util.List;

public class affichage implements Commande{
	boolean all;
	
	public affichage(boolean all)
	{
		this.all=all;
	}

	public String execute(List<Forme> Global, List<Forme> Select) {
	
		String res = new String();
		int i ;
		if(!all)
		{
			for(i=0;i<Select.size();i++)
			{
				res += "-Forme num : " + i + "\n"+Select.get(i).toString() + " \n\n";
			}
			return res;
		}
		else
		{
			for(i=0;i<Global.size();i++)
			{
				res += "-Forme num : " + i + "\n"+Global.get(i).toString() + " \n\n";
			}
			return res;
		}
	}

}
