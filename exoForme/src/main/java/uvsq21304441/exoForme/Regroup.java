package uvsq21304441.exoForme;

import java.util.List;

public class Regroup implements Commande{

	String[] selection;
	
	public Regroup(String[] selection){
		this.selection=selection;
	}
	
	
	public String execute(List<Forme> Global, List<Forme> Select) {
		Select.clear();
		int i;
		int toAdd;
		for(i=0;i<selection.length;i++)
		{
			toAdd = Integer.parseInt(selection[i]);
			Select.add(i, Global.get(toAdd));
		}
		return "Les objets ont bien ete regroupes \n";
	}

}
