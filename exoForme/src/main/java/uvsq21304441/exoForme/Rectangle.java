package uvsq21304441.exoForme;

public class Rectangle extends Forme{
	private Point HG;
	private Point BD;
	
	Rectangle(Point HG, Point BD)
	{
		this.BD = BD;
		this.HG = HG;
	}
	Rectangle(Point HG)
	{
		this.BD = new Point();
		this.HG = HG;
	}
	Rectangle()
	{
		this.BD = new Point();
		this.HG = new Point();
	}
	public Point getHG() {
		return HG;
	}
	public void setHG(Point hG) {
		HG = hG;
	}
	public Point getBD() {
		return BD;
	}
	public void setBD(Point bD) {
		BD = bD;
	}
	@Override
	public String toString() {
		return "Rectangle Point 1 : x= " + HG.getX() + " y= "+ HG.getY() + " \n Point 2 : x= " + BD.getX() + " y= "+ BD.getY();
	}
	@Override
	public String getclass() {	
		return "Rectangle";
	}
	@Override
	public void deplace(double dX, double dY) {
		this.HG.deplace(dX, dY);
		this.BD.deplace(dX, dY);
	}
}
