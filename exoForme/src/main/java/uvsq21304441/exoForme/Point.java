package uvsq21304441.exoForme;

import java.io.Serializable;

public class Point implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double x;
	private double y;
	
	Point()
	{
		this.y=0;
		this.x=0;
	}
	Point(double x)
	{
		this.x=x;
		this.y=0;
	}
	Point(double x , double y){
		this.x=x;
		this.y=y;
	}
	public double getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	public void deplace(double dX,double dY)
	{
		this.x +=dX;
		this.y +=dY;
	}
}
