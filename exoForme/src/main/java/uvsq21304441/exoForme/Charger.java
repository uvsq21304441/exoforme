package uvsq21304441.exoForme;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class Charger implements Commande {
	private String path;
	public Charger(String path)
	{
		this.path = path;
		
	}
	/**TODO MARTIN 
	 * 
	 * Ajouter un attribut PATH qui sera transmit par l'UI
	 * Init via un constructeur le PATH
	 * (CHECK LES AFFICHAGE, DEPALACEMNT ET APP pour avoir un exemple 
	 */
	
	
	public String execute(List<Forme> Global, List<Forme> Select){
		/**
		 * TODO MARTIN 
		 * Supprimer tout les objs dans Global et Select
		 * Puis créer les objs forme(rond, rectangle) via le fichier PATH et les ajouter dans la liste Global
		 * Return " Chargement réussi
		 * */
		Global.clear();
		Select.clear();
		ObjectInputStream ois;
		  try 
		  {
			  ois =new ObjectInputStream(new ObjectInputStream(new FileInputStream(path)));
			  try{
				  while(true)
				  {
					  Global.add((Forme)ois.readObject());
				  }
			  }

			  catch(EOFException e)
			  {	
				  	ois.close();
					return "Load completed \n";
			  }
		  } 
		  catch (FileNotFoundException e)
		  {
			  e.printStackTrace();
		  } 
		  catch (IOException e) 
		  {
				e.printStackTrace();
		} 
		  catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		return "Load completed \n";
	}
}
