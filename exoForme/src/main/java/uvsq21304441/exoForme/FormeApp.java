package uvsq21304441.exoForme;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class FormeApp {

	public static void main(String[] Args){
		FormeApp App = new FormeApp();
		App.run();
	}
	public void run(){
		List<Forme> Global = new ArrayList<Forme>();
		List<Forme> Select = new ArrayList<Forme>();
		
		FormeTUI ui = new FormeTUI();
		Commande c = null;
		
		Scanner sc = new Scanner(System.in);
		String input = sc.nextLine();
		
		boolean b = true;
	
		while(b){
			try{
				c = ui.NextCommande(input);
				ui.affiche(c.execute(Global,Select));
			}
			catch(NullPointerException e)
			{
				System.out.println("Commande inconnue \n");
			}
			catch(ArrayIndexOutOfBoundsException e)
			{
				System.out.println("Commande inconnue \n");
			}
			catch(IndexOutOfBoundsException e)
			{
				System.out.println("La forme n'existe pas \n");
			}
			finally{
				input = sc.nextLine();
			}
		}
		sc.close();
	}
}
