package uvsq21304441.exoForme;

import java.util.List;

public class Creer implements Commande{

	String[] obj;
	public Creer(String[] obj)
	{
		this.obj = obj;
	}
	
	public String execute(List<Forme> Global, List<Forme> Select) {
	try
	{
		if(obj[1].equals("cercle"))
		{
			Global.add(new Rond(Double.parseDouble(obj[2]),new Point(Double.parseDouble(obj[3]),Double.parseDouble(obj[4]))));
		}
		else{
			Global.add(new Rectangle(new Point(Double.parseDouble(obj[2]),Double.parseDouble(obj[3])),new Point(Double.parseDouble(obj[4]),Double.parseDouble(obj[5]))));
		}
		return "La forme "+ obj[1] + " a bien ete cree \n";
	}
	catch(NumberFormatException e)
	{
		return "Erreur d'argument";
	}
	}
}
