package uvsq21304441.exoForme;

public class FormeTUI {
	
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    if(Integer.parseInt(str)<256)
	    return true;
	    return false;
	    
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	}
	
	public Commande NextCommande(String input)
	{
		String[] parts = input.split(" ");
		
		if(parts.length==2 && parts[0].equals("sauvegarder"))
		{
			Sauvegarde save = new Sauvegarde(parts[1]);
			return save;
		}
		else if(parts.length==2 && parts[0].equals("charger"))
		{
			Charger load = new Charger(parts[1]);
			return load;
		}
		else if(parts.length==3 && parts[0].equals("deplacer"))
		{
			Deplacement d = new Deplacement(Double.parseDouble(parts[1]),Double.parseDouble(parts[2]));
			return d;
		}
		else if(parts.length==2 && parts[0].equals("afficher") && parts[1].equals("tout"))
		{
			affichage a = new affichage(true);
			return a;
		}
		else if(parts.length==1 && parts[0].equals("afficher"))
		{
			affichage a = new affichage(false);
			return a;
		}
		else if(isNumeric(parts[0])) //regroupement
		{
			Regroup r = new Regroup(parts);
			return r;
		}
		else if(input.equals("exit"))
		{
			Exit q = new Exit();
			return q;
		}else if(parts.length==5 && parts[0].equals("creer") && parts[1].equals("cercle"))
		{
			Creer d = new Creer(parts);
			return d;
		}
		else if(parts.length==6 && parts[0].equals("creer") && parts[1].equals("rectangle"))
		{
			Creer d = new Creer(parts);
			return d;
		}
		else if(input.equals("supprimer"))
		{
			Supprimer s = new Supprimer();
			return s;
		}
		return null;
	}
	
	public void affiche(String aff)
	{
		System.out.println(aff);	
	}
}
