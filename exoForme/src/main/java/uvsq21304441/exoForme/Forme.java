package uvsq21304441.exoForme;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class Forme implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public abstract String toString();
	public abstract String getclass();
	public abstract void deplace(double dX,double dY);
	
	
}
