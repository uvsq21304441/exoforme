package uvsq21304441.exoForme;

public class Rond extends Forme{
	private Point centre;
	double rayon;
	
	Rond()
	{	
		super();
		this.centre = new Point();
		this.rayon = 1.0;
	}
	Rond(Point centre)
	{
		this.centre = centre;
		this.rayon = 1.0;
	}
	Rond(double rayon)
	{
		this.centre = new Point();
		this.rayon = rayon;
	}
	Rond(double rayon,Point centre)
	{
		this.centre = centre;
		this.rayon = rayon;
	}
	public Point getCentre() {
		return centre;
	}
	public void setCentre(Point centre) {
		this.centre = centre;
	}
	public double getRayon() {
		return rayon;
	}
	public void setRayon(double rayon) {
		this.rayon = rayon;
	}
	@Override
	public String toString() {
		return "Rond de Rayon r = " + rayon + " Centre en x = "+ centre.getX() + " y = "+ centre.getY();
	}
	@Override
	public String getclass() {	
		return "Rond";
	}
	@Override
	public void deplace(double dX,double dY) {
		this.centre.deplace(dX, dY);
	}
}
