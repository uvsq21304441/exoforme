package uvsq21304441.exoForme;

import java.util.List;

public class Deplacement implements Commande{
	double dX;
	double dY;
	
	public Deplacement(double dX,double dY)
	{
		this.dX= dX;
		this.dY =dY;
	}
	public String execute(List<Forme> Global, List<Forme> Select) {
		int i;
		for(i=0;i<Select.size();i++)
		{
				Select.get(i).deplace(dX,dY);
		}
		return "Deplacement de dX = " + dX + " et dY = " + dY + " pour objet(s) selectione(s) e ! \n";
	}

}
